(function () {
  const burger = document.querySelector('.burger');
  const menu = document.querySelector('.wrapper-mobile');
  const page = document.querySelector('body');
  const content = document.querySelector('main');

  const showMobileMenu = () => {
    page.classList.toggle('fixed');
    content.classList.toggle('faded');
    burger.classList.toggle('cross');
    menu.classList.toggle('wrapper-mobile--opened');

  }



  burger.addEventListener('click', showMobileMenu);
}());