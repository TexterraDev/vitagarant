(function () {

  const buttons = document.querySelectorAll('.production__link');
  const card = document.querySelector('.production__card');

  const header = document.querySelector('.production__header');
  const text = document.querySelector('.production__text');
  const list = document.querySelector('.production__substances');
  const more = document.querySelector('.button--pop');


  const getData = (element) => {
    let string = element.getAttribute('data-json');
    let information = JSON.parse(string);

    return information
  }

  const renderCard = (element, evt) => {
    let data = getData(element);

    header.textContent = data.title;
    text.textContent = data.text;
    list.innerHTML = '';
    data.links.forEach((item) => {
      list.insertAdjacentHTML('beforeend', 
      `<li class="production__substance">
          <a class="production__sub-item" href="${item.link}">${item.title}</a>
        </li>`);
    });
    if($('.production__substance').length > 0){
      $('.production__include').addClass('vis');
    } else {
      $('.production__include').removeClass('vis');
    }
    more.href = data.button;

    element.disabled = true;
    
    buttons.forEach((item) => {
      if(item !== evt.currentTarget) {
        item.disabled = false;
      }
    });

    card.classList.add('production__card--active');
  }

  const getActive = (evt) => {
    evt.target.classList.add('production__link--active');
    buttons.forEach((element) => {
      if (element !== evt.target) {
        element.classList.remove('production__link--active');
      }
    })
  }

  buttons.forEach((element) => {
    element.addEventListener('click', (evt) => {
        renderCard(element, evt);
        getActive(evt);
      });
  });


}());