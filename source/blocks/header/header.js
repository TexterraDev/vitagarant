(function () {
  
  const header = document.querySelector('.header');
  const mobileMenu = document.querySelector('.wrapper-mobile');
  const navigation = document.querySelector('.navigation');

  let switcher = true;

  const transferNavigation = () => {
    if (window.matchMedia("(max-width: 959px)").matches && switcher === true) {
      mobileMenu.prepend(navigation);
      switcher = false;
    } else if (window.matchMedia("(min-width: 960px)").matches && switcher === false) {
      header.appendChild(navigation);
      switcher = true;
    }
    return switcher;
  }

  window.addEventListener('resize', transferNavigation);

  transferNavigation();
    
}());