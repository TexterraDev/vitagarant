(function () {
  const duoSlider = new Swiper ('.duoSlider__container', {
    slidesPerView: 1,
    loop: false,
    pagination: {
      el: '.duoSlider__pagination',
      clickable: true,
      dynamicBullets: true
    },
    navigation: {
      nextEl: '.duoSlider__button--next',
      prevEl: '.duoSlider__button--prev'
    },
    breakpoints: {
      600: {
        slidesPerView: 1,
        spaceBetween: 0,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
      }
    }
  });
}());