(function () {
  const filters = document.querySelectorAll('.findings__filter');

  const switchFilter = (evt) => {
    if (!evt.target.classList.contains('findings__filter--active')) {
      evt.target.classList.add('findings__filter--active');


      filters.forEach((element) => {
        if (element !== evt.target && element.classList.contains('findings__filter--active')) {
          element.classList.remove('findings__filter--active');
        }
      });
    }
  }

  filters.forEach((element) => {
    element.addEventListener('click', switchFilter)
  })
}());