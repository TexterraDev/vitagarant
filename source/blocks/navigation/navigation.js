(function () {
  const navigationList =  document.querySelector('.navigation__list');
  const navigationLinks = document.querySelectorAll('.navigation__link');

  const activateLink = (evt) => {
    evt.target.classList.add('navigation__link--active');

    navigationLinks.forEach((element) => {
      if (element !== evt.target) {
        element.classList.remove('navigation__link--active');
      }
    });
  }

  navigationList.addEventListener('click', (evt) => {
    if (evt.target.classList.contains('navigation__link')) {
      activateLink(evt);
    };
  });

}());