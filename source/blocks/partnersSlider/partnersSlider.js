(function () {
  const partnersSlider = new Swiper ('.partnersSlider__container', {
    slidesPerView: 4,
    spaceBetween: 50,
    loop: false,
    pagination: {
      el: '.partnersSlider__pagination',
      clickable: true,
      dynamicBullets: true
    },
    navigation: {
      nextEl: '.partnersSlider__button--next',
      prevEl: '.partnersSlider__button--prev'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 0,
      },
      600: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 30,
      },
      960: {
        slidesPerView: 4,
        spaceBetween: 50,
      }
    }
  });
}());