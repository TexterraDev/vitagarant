const getFormGreeted = (form) => {
  let apply = form.querySelector('.form__apply');
  let message = form.querySelector('.form__message');

  apply.classList.add('form__apply--hidden');
  message.classList.add('form__message--shown');
}

(function () {

  document.addEventListener("DOMContentLoaded", () => {

    const phones = document.querySelectorAll('.form__input--phone');
    const inputs = Array.from(document.querySelectorAll('.form__input'));
    const filteredInputs = inputs.filter((element) => {
      return element.attributes.type.nodeValue !== 'phone';
    });

    filteredInputs.forEach(function (element) {
      element.addEventListener('input', () => {
          if (element.value.length === '' || element.value.length <= 2) {
            element.classList.add('form__input--invalid');
            element.setCustomValidity('Введённое значение должно быть больше 2 символов');
          } else {
            element.classList.remove('form__input--invalid');
            element.setCustomValidity('');
          }
        });
      });

      function mask() {
        var matrix = "+7 (___) ___ ____",
            i = 0,
            def = matrix.replace(/\D/g, ""),
            val = this.value.replace(/\D/g, "");
        if (def.length >= val.length) val = def;
        this.value = matrix.replace(/./g, function(a) {
            return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
        });
      };

      if(phones) {


        phones.forEach((element => {
          element.addEventListener("input", mask);

          element.addEventListener('change', () => {
            if (element.value.length < 17) {
              element.classList.add('form__input--invalid');
              element.value = '';
            } else {
              element.classList.remove('form__input--invalid');
            }
          });
        }))
    }
  });

}());