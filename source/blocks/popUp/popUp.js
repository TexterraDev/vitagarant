(function () {
  const cross = document.querySelector('.popUp__close');
  const button = document.querySelector('.button--order');
  const overlay = document.querySelector('.overlay');
  const page = document.querySelector('body');


  const actionPopUp = () => {
    cross.parentElement.classList.toggle('hidden');
    overlay.classList.toggle('hidden');
    page.classList.toggle('fixed');
  }

  cross.addEventListener('click', actionPopUp);
  button.addEventListener('click', actionPopUp);
}());