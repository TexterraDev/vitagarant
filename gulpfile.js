const gulp = require('gulp');
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const babel = require('gulp-babel');
const replace = require('gulp-replace');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const svgSprite = require('gulp-svg-sprite');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const csso = require('gulp-csso');
const minify = require('gulp-minify');

let jsLibs = [
  'node_modules/jquery/dist/jquery.js', // Jquery
  'node_modules/swiper/js/swiper.min.js', // SwiperSlider
  'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js' // Fancybox
]

let cssLibs = [
  'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css', // Fancybox
  'node_modules/swiper/css/swiper.min.css' // SwiperSlider
]

function style() {
  return gulp.src(cssLibs)
    .pipe(concat('libs.css'))
    .pipe(csso())
    .pipe(gulp.dest('build/css')),

  gulp.src('source/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
    .pipe(csso())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.stream())
}

function html() {
  return gulp.src('source/pages/**/*.pug')
    .pipe(pug({
      pretty: true,
      basedir: __dirname + '/'
    }))
    .pipe(gulp.dest('build/'))
    .pipe(browserSync.stream())
}

function fontsTransfer() {
  return gulp.src('source/assets/fonts/**/*.*')
    .pipe(gulp.dest('build/fonts'))
}

function bundleJS() {
  return gulp.src(jsLibs)
    .pipe(concat('libs.js'))
    .pipe(minify({
      noSource: true
    }))
    .pipe(rename("libs.js"))
    .pipe(gulp.dest('build/js')),

  gulp.src('source/blocks/**/*.js')
    .pipe(concat('scripts.js'))
    .pipe(babel({
			presets: ['@babel/preset-env']
    }))
    .pipe(minify({
      noSource: true
    }))
    .pipe(rename("scripts.js"))
    .pipe(gulp.dest('build/js'));
};

function imageTransfer() {
  return gulp.src('source/assets/images/*.*')
    .pipe(gulp.dest('build/images')),

  gulp.src('source/assets/icons/favicon/favicon.ico')
    .pipe(gulp.dest('build/'))
}

function icons() {
  return gulp.src('source/assets/icons/*.svg')
      .pipe(replace('&gt;', '>'))
      .pipe(rename({prefix: 'icon-'}))
      .pipe(svgSprite({
          mode: {
              symbol: {
                  dest: '',
                  sprite: 'icons.svg'
              }
          },
          svg: {
              xmlDeclaration: false,
              doctypeDeclaration: false,
              namespaceIDs: false,
              dimensionAttributes: false
          }
      }))
      .pipe(gulp.dest('build/images/icons/'));
}

function watch() {
  browserSync.init({
    server: {
      baseDir: './build',
    }
  });
  gulp.watch('source/**/*.scss', style);
  gulp.watch('source/**/*.pug', html).on('change', browserSync.reload);
  gulp.watch('source/assets/**/*.{png,jpg,jpeg,gif,webp,svg}', gulp.series(icons, imageTransfer));
  gulp.watch('source/**/*.js', gulp.series(bundleJS)).on('change', browserSync.reload);
}

gulp.task('build', gulp.series(icons, style, imageTransfer, fontsTransfer, bundleJS, html, function (done) {
    done();
  })
);

gulp.task('watch:dev', gulp.series('build',  gulp.parallel(watch)));

exports.style = style
exports.html = html
exports.bundleJS = bundleJS
exports.icons = icons
exports.fontsTransfer = fontsTransfer
exports.imageTransfer = imageTransfer
exports.watch = watch